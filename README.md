A Python 2.7 project to enable 826-Michigan to write CSV files with students and print them using the Choosatron (https://choosatron.com/)

Requires usage of the twine-export-story (https://github.com/jerrytron/twine-story-export) library.