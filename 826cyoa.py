import sys
import csv
import re


def main():
    """Entry point"""
    csvFile = read_csv()
    storyMap = get_entries_and_options_from_csv(csvFile)
    write_twine_exportable_file(storyMap)


def read_csv():
    """Pull in CSV file and pass to appropriate function"""
    return open('cyoa.csv', 'rt')


def get_entries_and_options_from_csv(csvFile):
    """Create in memory list of story from 826_cyoa_template_sheet"""
    reader = csv.reader(csvFile, delimiter=',')
    resultMap = []
    isFirstRow = True
    isSecondRow = True
    for row in reader:
        for index, item in enumerate(row):
            if index == 0 or isFirstRow:
                continue
            elif isSecondRow:
                resultMap.append(Page(index, item))
            else:
                page = resultMap[index - 1]
                if page is not None:
                    page.set_option(item)
        if not isFirstRow:
            isSecondRow = False
        isFirstRow = False
    # print_result_map(resultMap)
    return resultMap


def write_twine_exportable_file(storyMap):
    with open("Output.txt", "w") as textFile:
        for page in storyMap:
            textFile.write("::{}{}\n\n{}".format(page.pageNum,
                                                 page.content,
                                                 print_options(page)))


def print_options(page):
    result = ""
    for option in page.options:
        if option.content is not None:
            if option.goToNumber is None:
                result += "[[{}]]".format(option.content)
            else:
                result += "[[{}|Page {}]]\n".format(option.content,
                                                    option.goToNumber)
    return result


def print_result_map(resultMap):
    for page in resultMap:
        print("Page#: {} Content: {}".format(page.pageNum, page.content))
        for option in page.options:
            print("Content: {}, GOTO: {}".format(option.content,
                                                 option.goToNumber))


def stripNonIntegerFromString(s):
    return int(re.sub('[^0-9]', '', s))

# Classes


class Page:
    def __init__(self, pageNum, content):
        self.pageNum = self.format_page_num(pageNum)
        self.content = content
        self.options = []

    def set_option(self, option):
        self.options.append(Option(option))

    def format_page_num(self, pageNum):
        return "Start" if pageNum == 1 else "Page {}".format(pageNum)


class Option:
    def __init__(self, content):
        self.content = content
        self.goToNumber = self.parseGoToNumber(content)
        if self.goToNumber is None:
            self.content = None

    def parseGoToNumber(self, content):
        content = content.replace(" ", "").lower()
        indexOfGoTo = content.find("gotopage")
        lengthOfGoToString = 8
        pageNumIndex = -1

        if indexOfGoTo == -1:
            indexOfGoTo = content.find("gotopg")
            lengthOfGoToString = 6
        if indexOfGoTo == -1:
            indexOfGoTo = content.find("gotopg.")
            lengthOfGoToString = 7
        if indexOfGoTo == -1:
            return None

        pageNumIndex = indexOfGoTo + lengthOfGoToString
        return stripNonIntegerFromString(content[pageNumIndex:]) or None

# Tests


# def test_is_csv_imported():
#     """Is the csv pulled in as a variable?"""
#     assert read_csv() is True


# def test_printing_csv():
#     """Is the csv's rows printed"""
#     csvFile = read_csv()
#     assert get_entries_and_options_from_csv(csvFile)

# def test_writing_file():
#     """Is the file written properly"""
#     csvFile = read_csv()
#     storyMap = get_entries_and_options_from_csv(csvFile)
#     assert write_twine_exportable_file(storyMap)


def test_page_numbers_greater_than_nine():
    """Is the option go to number capable of parsing above 9"""
    option = Option("Go to page 10")
    assert option.goToNumber == 10

    option = Option("Go to page 103783586 ")
    assert option.goToNumber == 103783586


if __name__ == '__main__':
    sys.exit(main())
